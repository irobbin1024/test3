//
//  XYAppDelegate.h
//  test3
//
//  Created by irobbin1024 on 07/03/2019.
//  Copyright (c) 2019 irobbin1024. All rights reserved.
//

@import UIKit;

@interface XYAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
