//
//  main.m
//  test3
//
//  Created by irobbin1024 on 07/03/2019.
//  Copyright (c) 2019 irobbin1024. All rights reserved.
//

@import UIKit;
#import "XYAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XYAppDelegate class]));
    }
}
