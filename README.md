aa# test3

[![CI Status](https://img.shields.io/travis/irobbin1024/test3.svg?style=flat)](https://travis-ci.org/irobbin1024/test3)
[![Version](https://img.shields.io/cocoapods/v/test3.svg?style=flat)](https://cocoapods.org/pods/test3)
[![License](https://img.shields.io/cocoapods/l/test3.svg?style=flat)](https://cocoapods.org/pods/test3)
[![Platform](https://img.shields.io/cocoapods/p/test3.svg?style=flat)](https://cocoapods.org/pods/test3)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

test3 is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'test3'
```

## Author

irobbin1024, longbin.lai@quvideo.com

## License

test3 is available under the MIT license. See the LICENSE file for more info.
