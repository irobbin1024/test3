#!/bin/sh

export LANG=en_US.UTF-8

package_framework() {
    podName=$1
    # 1.0.4
    version=$2 
    packageDIR=$3
    new_version=$version
    frameworkHomeDir=${XYHOME}/SRC/FrameworkHub/${podName}/${new_version}
    
    echo ${frameworkHomeDir}

    cd ${packageDIR}

    grep "smart_version" ./${podName}.podspec > /dev/null
    smart_version_result=$?
    if [ $? -eq 0 ]; then
        echo "Found!"
    else
        echo "Not found!"
    fi


    #echo ${packageDIR}
    # 替换版本号
    if [ $smart_version_result -eq 0 ]; then
        sed -i '' "s/=\ smart_version/=\ \'$version\'/g" ./${podName}.podspec
    fi

    # 开始打包
    use_source=1 /usr/local/bin/pod package --spec-sources=http://192.168.1.33:9090/Pods/Specs.git,https://cdn.cocoapods.org/ --no-mangle --embedded --force --exclude-deps --configuration=Release ./${podName}.podspec
    if [ $? -ne 0 ]; then
        echo '打包失败'
        if [ $smart_version_result -eq 0 ]; then
            # 恢复版本号
            sed -i '' "s/=\ \'$version\'/=\ smart_version/g" ./${podName}.podspec
        fi
        exit 1 
    fi

    if [ $smart_version_result -eq 0 ]; then
        # 恢复版本号
        sed -i '' "s/=\ \'$version\'/=\ smart_version/g" ./${podName}.podspec
    fi

    rm -rf ./Framework/*
    mkdir -p ${frameworkHomeDir}
    echo "新建目录${frameworkHomeDir}"
    if [ ! -d ${frameworkHomeDir} ]; then
        echo 'mkdir ~~~'
        mkdir -p ${frameworkHomeDir}
    fi

    # 拷贝Framework
    /bin/cp -rf ./${podName}-$version/ios/${podName}.embeddedframework/*.framework $frameworkHomeDir
    # 移除打包临时目录
    rm -rf ./${podName}-$version
}

cd $1

packageDIR=$1
frameworkHomeDir=$2
podName=${packageDIR##*/}
version=`git describe --abbrev=0 --tags 2>/dev/null`

package_framework $podName $version $packageDIR $frameworkHomeDir
