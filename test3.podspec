#
# Be sure to run `pod lib lint test3.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  def self.smart_version
    tag = `git describe --abbrev=0 --tags 2>/dev/null`.strip
    if $?.success? then tag else "0.0.1" end
  end
  s.name             = 'test3'
  s.version          = smart_version
  s.summary          = 'XiaoYing - test3'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
XiaoYing pod  - test3
                       DESC
  s.homepage         = 'git@192.168.1.33:ioscomponentgroup/test3'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'irobbin1024' => 'longbin.lai@quvideo.com' }
  s.source           = { :git => 'git@192.168.1.33:ioscomponentgroup/test3.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  s.ios.deployment_target = '9.0'
  
  # =============================== ⬇️️️⬇️️️⬇️️️⬇️️️⬇️️️⬇️️️以下部分是Ruby脚本，控制Framework切换，请勿随意变更 ==============================
  s.preserve_paths = "#{s.name}/Classes/**/*","Framework/**/*", "#{s.name}/Assets/**/*",
  
  $source = ENV['use_source']
  $source_name = ENV["#{s.name}_use_source"]
  xy_home = ENV['XYHOME']

  $use_source=nil
  if $source_name=='1'
    $use_source = true
  elsif $source_name=='0'
    $use_source = false
  else
    if $source == '1'
      $use_source = true
    end
  end

  tag = `git describe --abbrev=0 --tags 2>/dev/null`.strip
  if tag && !tag.empty?
    $use_source=true
  end

  if $use_source==false
    # 是否有framework
    $use_source = !File.exist?("#{xy_home}/SRC/FrameworkHub/#{s.name}/#{s.version}/#{s.name}.framework")
  end

  if $use_source==true
    # ！！！！！！源码方式，需要加载哪些代码和资源，请在这里做相应变更 
    s.source_files = "#{s.name}/Classes/**/*"
  else
    # ！！！！！！以下为固定写法，理论上不要动它
    s.vendored_frameworks = "Framework/#{s.version}/*.framework"
  end

  # 以下是资源部分，基本不需要变更，如果你的资源不叫这个名字，请修改
  s.resource_bundles = {
    'test3' => ['test3/Assets/*.xcassets', 'test3/Assets/**/*.*']
  }

  # =============================== ⬆️️⬆️️⬆️️⬆️️⬆️️⬆️️ 以上部分是Ruby脚本，控制Framework切换，请勿随意变更 ==============================

  s.dependency 'XYToolKit'
  s.dependency 'XYCategory'
  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
